#ifndef PROCESSOR_H
#define PROCESSOR_H

#include "ebit.h"
#include "eword.h"

#include <vector>

class ProcessorHE
{
private:
    static EBit * s_oneBias;
    static EBit * s_zeroBias;

public:
    static const EBit & getZeroBias();
    static const EBit & getOneBias();

public:
    ProcessorHE();

    void    setSK( fhe_sk_t    sk );
    void    setDebugMode( bool debug ){ m_debug = debug; }

    bool    initialize(  fhe_pk_t    pk );
    void    deinitialize();

    void    setParams( int memoryWordSize, int operandSize, int instrucionIDSize );

    void    printInstr( EWord & instrID );
    void    memoryDmp( );

    void    loadDataMemory( const std::vector< EWord > & memory );
    void    loadCodeMemory( const std::vector< EWord > & memory );

    std::vector< EWord > & getDataMemory() { return m_dataMemory; }

    void    run();

private:
    void _debug();

private:
    bool        m_debug;

    fhe_pk_t    m_pk;

    bool        m_hasSK;
    fhe_sk_t    m_sk;

    std::vector< EWord > m_dataMemory;
    std::vector< EWord > m_codeMemory;

    int m_dataWordSize;         //rozmiar slowa danych pamieci
    int m_operandSize;          //rozmiar operandu
    int m_instructionIDSize;    //rozmiar czesci identyfikujacej rodzaj instrukcji
    int m_instructionSize;      //rozmiar calej instrukcji
    int m_registerSize;         //rozmiar rejestru
    int m_counterSize;          //rozmiar licznika instrukcji
    int m_functionsCount;       //liczba mozliwych instrukcji (czyli roznych funkcji)

    int m_dataMemorySize;       //liczba slow w pamieci danych
    int m_codeMemorySize;       //liczba slow w pamieci kodu

    bool    m_paramsSet;
};

#endif // PROCESSOR_H

