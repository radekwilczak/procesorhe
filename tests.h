#ifndef TESTS_H
#define TESTS_H

#include <assert.h>

#include "integer-fhe.h"
#include "ebit.h"
#include "eword.h"
#include "testutils.h"
#include "processor.h"
#include "miscfunctions.h"
#include "memory.h"

class Tests
{
public:
    Tests();

    //piaskownica
    void poligoon( bool debug );
    void dirtTest();
    void dirtTest2();

    //odpalenie testow jednostkowych
    void    tests();

    //pomocnicze
    void    initTests();
    void    deinitTests();


    //TESTY JEDNOSTKOWE:
    void    test_EBit_enc_dec();
    void    test_EBit_copy_assign();
    void    test_EBit_and_xor();


    void    test_EWord_enc_dec();
    void    test_EWord_copy();
    void    test_EWord_encodeDecodeInt();

	void	test_aluAdd();
    void    test_aluMul();

    void    test_CtrlEq();
    void    test_ctrlJmp();
    void    test_ctrlGr();

    void    test_MiscINC();
    void    test_MiscMUX();
    void    test_MiscShiftN();

    void    test_memRead();
    void    test_memWrite();

    void    test_toFile();


private:
    fhe_pk_t    pk;
    fhe_sk_t    sk;

    ProcessorHE processor;

};

#endif // TESTS_H
