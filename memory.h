#ifndef MEMORY_H
#define MEMORY_H

#include <vector>

#include "eword.h"
#include "ebit.h"

void    memWrite( std::vector< EWord > & memory, EWord & in, EWord & select, const EBit & chipSelect );

void    memRead( EWord  & out, const std::vector< EWord > & memory, EWord & select, const EBit & chipSelect );

#endif // MEMORY_H
