#ifndef CONTROLFUNCTIONS_H
#define CONTROLFUNCTIONS_H
#include "eword.h"
#include "ebit.h"


//if A==R then F(0) <= 1
void ctrlEq( EWord & resultF, EWord & inA, EWord & inR, EWord & inF, EBit & chipSelect );

//if A<R then F(0) <= 1
void ctrlGr( EWord & resultF, EWord & inA, EWord & inR, EWord & inF, EBit & chipSelect );

//if F(0) == 1 then CNT <= op
void ctrlJmp( EWord & resultCNT, EWord &inCNT, EWord & inF, EWord & op, EBit & chipSelect );


void    ctrlOp( EWord & resultR, EWord & op, EBit & chipSelect );

void 	ctrlMove(  EWord & to, EWord & from, EBit & chipSelect );

//Zamien rejestr F z A (F<=>A)
void	ctrlSwpf( EWord & resultA, EWord & resultF, EWord & inA, EWord & inF, EBit & chipSelect  );



#endif // CONTROLFUNCTIONS_H
