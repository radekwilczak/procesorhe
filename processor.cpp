#include "processor.h"

#include <iostream>
#include <math.h>

#include "alufunctions.h"
#include "miscfunctions.h"
#include "controlfunctions.h"
#include "memory.h"


EBit * ProcessorHE::s_oneBias = NULL;
EBit * ProcessorHE::s_zeroBias = NULL;

const EBit &ProcessorHE::getZeroBias()
{
    if( s_zeroBias == NULL )
    {
        std::cerr << "ERROR: ProcessorHE not initialized!";
        assert( false );
    }
    return *s_zeroBias;
}

const EBit &ProcessorHE::getOneBias()
{
    if( s_oneBias == NULL )
    {
        std::cerr << "ERROR: ProcessorHE not initialized!";
        assert( false );
    }
    return *s_oneBias;
}



ProcessorHE::ProcessorHE()
    :   m_paramsSet( false ),
        m_hasSK( false ),
        m_debug( false )
{
}

void ProcessorHE::setSK(fhe_sk_t sk)
{
    m_sk[0] = sk[0];
    m_hasSK  = true;
}


bool ProcessorHE::initialize( fhe_pk_t    pk )
{
    m_pk[0] = pk[0];
    s_oneBias = new EBit( m_pk, 1 );
    s_zeroBias = new EBit( m_pk, 0 );
}

void ProcessorHE::deinitialize()
{
    delete s_oneBias;
    delete s_zeroBias;
    s_oneBias = NULL;
    s_zeroBias = NULL;
}

void ProcessorHE::setParams(int memoryWordSize, int operandSize, int instrucionIDSize)
{
    m_dataWordSize  = memoryWordSize;
    m_operandSize   = operandSize;
    m_instructionIDSize = instrucionIDSize;
    m_instructionSize   = m_operandSize + m_instructionIDSize;
    m_registerSize      = m_dataWordSize;
    m_functionsCount    = pow( 2 , m_instructionIDSize );

    m_paramsSet = true;
}

void ProcessorHE::printInstr(EWord &instrID)
{
    if( !m_hasSK )
        return;
    char buffer[256];
    unsigned int instr = instrID.decodeUInt( m_sk );
    switch( instr )
    {
        case 0:
            sprintf( buffer, "%s", "Shift left (shl)");
            break;
        case 1:
            sprintf( buffer, "%s", "Shift right (shr)");
            break;
        case 2:
            sprintf( buffer, "%s", "XOR (xor)");
            break;
        case 3:
            sprintf( buffer, "%s", "AND (and)");
            break;
        case 4:
            sprintf( buffer, "%s", "NOT (not)");
            break;
        case 5:
            sprintf( buffer, "%s", "Equation (eq)");
            break;
        case 6:
            sprintf( buffer, "%s", "Swap F and A (swpf)");
            break;
        case 7:
            sprintf( buffer, "%s", "Add (add)");
            break;
        case 8:
            sprintf( buffer, "%s", "Mul (mul)");
            break;
        case 9:
            sprintf( buffer, "%s", "Set op to R (setr)");
            break;
        case 10:
            sprintf( buffer, "%s", "Move R to A (movr)");
            break;
        case 11:
            sprintf( buffer, "%s", "Move A to R (mova)");
            break;
        case 12:
            sprintf( buffer, "%s", "Read memory (memrd)");
            break;
        case 13:
            sprintf( buffer, "%s", "Write memory (memwr)");
            break;
        case 14:
            sprintf( buffer, "%s", "Greater-than (gr)");
            break;
        case 15:
            sprintf( buffer, "%s", "Conditional jump (jmp)");
            break;
    }

    printf( "Next instruction: %s\n", buffer );
    fflush( stdout );
}

void ProcessorHE::memoryDmp()
{
    printf( "Memory dump:\n");
    for( int i = 0 ; i < m_dataMemorySize ; ++i )
    {
        int value = m_dataMemory[i].decodeUInt( m_sk );
        printf( "data memory[%d] = %d\n", i, value );
        fflush( stdout );
    }

}

void ProcessorHE::loadDataMemory(const std::vector<EWord> &memory)
{
    int memSize = memory.size();
    assert( memSize > 0 );
    assert( m_paramsSet == true );

    for( int i = 0 ;i < memSize ; ++i )
    {
        assert( memory[ i ].size() == m_dataWordSize );
    }

    m_dataMemory        = memory;
    m_dataMemorySize    = memSize;
}

void ProcessorHE::loadCodeMemory(const std::vector<EWord> &memory)
{
    int memSize = memory.size();
    assert( memSize > 0 );
    assert( m_paramsSet == true );

    for( int i = 0 ;i < memSize ; ++i )
    {
        assert( memory[ i ].size() == m_instructionSize );
    }

    m_codeMemory        = memory;
    m_codeMemorySize    = memSize;
    m_counterSize       = log2( memSize ) +1;
    m_counterSize       = std::max( m_counterSize, m_operandSize );
}

void ProcessorHE::run()
{
    assert( m_dataMemory.size() > 0 );
    assert( m_codeMemory.size() > 0 );
    assert( m_paramsSet == true );

    EWord   A( m_pk, m_registerSize, 0 );
    EWord   R( m_pk, m_registerSize, 0 );
    EWord   F( m_pk, m_registerSize, 0 );

    EWord   resultA( m_pk, m_registerSize, 0 );
    EWord   resultF( m_pk, m_registerSize, 0 );
    EWord   resultR( m_pk, m_registerSize, 0 );
    EWord   resultCNT( m_pk, m_counterSize, 0 );

    EWord   tempA( m_pk, m_registerSize, 0 );
    EWord   tempF( m_pk, m_registerSize, 0 );
    EWord   tempR( m_pk, m_registerSize, 0 );
    EWord   tempCNT( m_pk, m_counterSize, 0 );

    EWord   CNT( m_pk, m_counterSize, 0 );


    EWord   codeWord( m_pk, m_instructionSize, 0 );
    EWord   codeID( m_pk, m_instructionIDSize, 0 );
    EWord   codeOp( m_pk, m_operandSize, 0 );
    EWord   codeSelect( m_pk, m_codeMemorySize, 0 );
    EWord   functionSelect( m_pk, m_functionsCount, 0 );
    EWord   dataMemorySelect( m_pk, m_dataMemorySize, 0 );

	// maska, ktora posluzy do ustalenia, czy dany rejestr lub pamiec sa modyfikowane
    int     instructionsA[]   = { 0, 1, 2, 3, 4, 6, 7, 8, 10 };
    int     instructionsACount  = sizeof( instructionsA )/ sizeof( int );

    int     instructionsF[]   = { 5, 6 };
    int     instructionsFCount  = sizeof( instructionsF )/ sizeof( int );

    int     instructionsR[]   = { 9, 11, 12 };
    int     instructionsRCount  = sizeof( instructionsR )/ sizeof( int );

    int     instructionsCNT[]   = { 15 };
    int     instructionsCNTCount  = sizeof( instructionsCNT )/ sizeof( int );

    int     instructionsMemWrite[]   = { };
    int     instructionsMemWriteCount  = sizeof( instructionsMemWrite )/ sizeof( int );

    for( int i = 0 ; i < m_codeMemorySize + 100; ++i )
    {
        printf( "Begin processor cycle\n" );

        int decCNT = CNT.decodeUInt( m_sk );
		//program konczy sie gdy licznik osiagnie za ostatnia instrukcje
        if( decCNT == m_codeMemorySize )
        {
            printf ( "program stop!\n" );
            break;
        }

        printf( "CNT:" );
        CNT.print( m_sk );

        printf( "A:" );
        A.print( m_sk );
        printf( "R:" );
        R.print( m_sk );
        printf( "F:" );
        F.print( m_sk );

        miscMUX( codeSelect, CNT );
        //printf( "codeSelect:" );
        //codeSelect.print( m_sk );

        memRead( codeWord, m_codeMemory, codeSelect, EBit( m_pk, 1 ) );
        printf( "codeWord:" );
        codeWord.print( m_sk );

        codeWord.copyTo( codeID, 0, 0, m_instructionIDSize );
        codeWord.copyTo( codeOp, m_instructionIDSize, 0, m_operandSize );

        printInstr( codeID );

		//obsluga debug
        _debug();

        //printf( "instr ID:" );
        //codeID.print( m_sk );
        //printf( "instr OP:" );
        //codeOp.print( m_sk );

        miscMUX( functionSelect, codeID );
        //printf( "functionSelect:" );
        //functionSelect.print( m_sk );

        A.copyTo( tempA );
        EBit    AToBeModyfied = miscAnyBitOne( functionSelect, instructionsA, instructionsACount);
        tempA.bitAND( AToBeModyfied );

        F.copyTo( tempF );
        EBit    FToBeModyfied = miscAnyBitOne( functionSelect, instructionsF, instructionsFCount);
        tempF.bitAND( FToBeModyfied );

        R.copyTo( tempR );
        EBit    RToBeModyfied = miscAnyBitOne( functionSelect, instructionsR, instructionsRCount);
        tempR.bitAND( RToBeModyfied );

        CNT.copyTo( tempCNT );
        //EBit    CNTToBeModyfied = miscAnyBitOne( functionSelect, instructionsCNT, instructionsCNTCount);
        //tempCNT.bitAND( CNTToBeModyfied );

        //alu
        aluShiftLeft( resultA, R, functionSelect[0] );
        //printf( "aluShiftLeft:" );
        //resultA.print( m_sk );
        tempA.elementwiseXOR( resultA );

        aluShiftRight( resultA, R, functionSelect[1] );
        //printf( "aluShiftRight:" );
        //resultA.print( m_sk );
        tempA.elementwiseXOR( resultA );

        aluXOR( resultA, A, R, functionSelect[2] );
        //printf( "aluXOR:" );
        //resultA.print( m_sk );
        tempA.elementwiseXOR( resultA );

        aluAND( resultA, A, R, functionSelect[3] );
        //printf( "aluAND:" );
        //resultA.print( m_sk );
        tempA.elementwiseXOR( resultA );

        aluNOT( resultA, R, functionSelect[4] );
        //printf( "aluNOT:" );
        //resultA.print( m_sk );
        tempA.elementwiseXOR( resultA );

        aluAdd( resultA, A, R, functionSelect[ 7 ]);
        //printf( "aluAdd:" );
        tempA.elementwiseXOR( resultA );

        aluMul( resultA, A, R, functionSelect[ 8 ]);
        //printf( "aluMul:" );
        tempA.elementwiseXOR( resultA );


        //mem
        miscMUX( dataMemorySelect, codeOp );
        //printf( "dataMemorySelect:" );
        //dataMemorySelect.print( m_sk );
        memRead( resultR, m_dataMemory, dataMemorySelect , functionSelect[ 12 ] );
        tempR.elementwiseXOR( resultR );

        memWrite( m_dataMemory, A, dataMemorySelect, functionSelect[ 13 ] );

        //ctrl
        ctrlMove( resultA, R, functionSelect[ 10 ] );
        tempA.elementwiseXOR( resultA );

        ctrlMove( resultR, A, functionSelect[ 11 ] );
        tempR.elementwiseXOR( resultR );

        ctrlEq( resultF, A, R, F, functionSelect[5] );
        tempF.elementwiseXOR( resultF );

        ctrlSwpf( resultA, resultF, A, F, functionSelect[6] );
        tempF.elementwiseXOR( resultF );
        tempA.elementwiseXOR( resultA );

        ctrlOp( resultR, codeOp, functionSelect[ 9 ] );
        tempR.elementwiseXOR( resultR );

        ctrlGr( resultF, A, R, F, functionSelect[14] );
        tempF.elementwiseXOR( resultF );

        ctrlJmp( resultCNT, CNT, F, codeOp, functionSelect[15] );
        //printf( "ctrlJmp:" );
        //resultCNT.print(m_sk);
        tempCNT.elementwiseXOR( resultCNT );

        //printf( "tempA:" );
        //tempA.print( m_sk );
        A.elementwiseXOR( A, tempA );
        F.elementwiseXOR( F, tempF );
        R.elementwiseXOR( R, tempR );

        EBit nonJmp = ~( functionSelect[15] * F[0] );
        miscINC( resultCNT, CNT,  nonJmp );
        tempCNT.elementwiseXOR( resultCNT );

        CNT.elementwiseXOR( CNT, tempCNT );
    }

    memoryDmp();
}

void ProcessorHE::_debug()
{
    if( !m_debug )
        return;
    printf( "debug mode:\n" );
    fflush( stdout );

    bool getNext = true;
    while( getNext )
    {
        char c = getchar();
        fflush( stdout );
        if( c == '\n' )
            continue;

        if( c == 'n' )
            break;

        if( c == 'd')
        {
            memoryDmp();
            continue;
        }

        if( c == 'c' )
        {
            m_debug = false;
            break;
        }
    }
}

