#include "testutils.h"

bool TestUtils::checkWord(fhe_sk_t sk, EWord &w, int bits[], int offset, int size)
{
    if( size < 0 )
        size = w.size();

    for( int i = offset ; i < size; ++i )
    {
        bits[ i ] = ( bits[ i ] != 0 ? 1 : 0 );//normalizacja do 1/0

        if( bits[ i ] !=  w[i].decrypt( sk ) )
            return false;
    }

    return true;
}

bool TestUtils::checkWord(fhe_sk_t sk, EWord &w, unsigned int value)
{
    unsigned int len = w.size();

    for( int j = 0 ; j < len ; ++ j)
    {
        int bit = value & 0x01;
        value = value >> 1;
        if( w[j].decrypt( sk ) != bit )
        {
            return false;
        }
    }

    return true;
}
