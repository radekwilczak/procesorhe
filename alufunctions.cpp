#include "alufunctions.h"
#include "processor.h"

void 	aluShiftLeft( EWord & result, EWord & in,  EBit & chipSelect )
{
    //szerokosc wej. musi byc rowna szerokosci wyj.
    assert( result.size() == in.size() );

    //podpiecie odpowiednich linii wej. do odpowiednich lini wyjscia
    for( int i = 1 ; i < in.size() ; ++i )
    {
        result[i] = in[i-1];
    }
    result[ 0 ] = in[0] + in[0];    // xor tego samego bitu = zero

    //jesli uklad nie jest 'wybrany' to wyjscie jest zerem.
    result.bitAND( chipSelect );
}

void 	aluShiftRight( EWord & result, EWord & in,  EBit & chipSelect )
{
	//szerokosc wej. musi byc rowna szerokosci wyj.
    assert( result.size() == in.size() );

	int lastElementIndex = in.size()-1;

    //podpiecie odpowiednich linii wej. do odpowiednich lini wyjscia
    for( int i = 0 ; i < lastElementIndex ; ++i )
    {
        result[i] = in[i+1];
    }
    result[ lastElementIndex ] = in[0] + in[0];    // xor tego samego bitu = zero

    //jesli uklad nie jest 'wybrany' to wyjscie jest zerem.
    result.bitAND( chipSelect );
}

void 	aluXOR( EWord & resultA, EWord & inA, EWord & inR, EBit & chipSelect )
{
	inA.elementwiseXOR(resultA, inR);
	
	//jesli uklad nie jest 'wybrany' to wyjscie jest zerem.
    resultA.bitAND( chipSelect );
}

void    aluAND( EWord & resultA, EWord & inA, EWord & inR, EBit & chipSelect )
{
	//szerokosc wej. musi byc rowna szerokosci wyj.
    assert( inA.size() == inR.size() );
    
    for( int i = 0 ; i < inA.size() ; ++i )
    {
    	resultA[ i ] = inA[i] * inR[i];
	}
	
	//jesli uklad nie jest 'wybrany' to wyjscie jest zerem.
    resultA.bitAND( chipSelect );
}

void    aluNOT( EWord & result, EWord & in, EBit & chipSelect )
{    
	in.negated(result);
    
	//jesli uklad nie jest 'wybrany' to wyjscie jest zerem.
    result.bitAND( chipSelect );
}




void	aluAdd( EWord & resultA, EWord & inA, EWord & inR, EBit & chipSelect  )
{
	// wszystkie rejestry muszą mieć taki sam rozmiar
	assert( inA.size() == inR.size() );
	assert( resultA.size() == inA.size() );
	
	EBit carryIn = ProcessorHE::getZeroBias();
		
	for (int i = 0 ; i < inA.size() ; ++i) 
	{
		EBit carryOut = ProcessorHE::getZeroBias();
		EBit::fulladd( resultA[i], carryOut, inA[i], inR[i], carryIn );
        resultA[i].recrypt();
        carryOut.recrypt();
        //TODO: nie jestem pewien ale tu raczej trzeba zrobic recrypt na result i carryOut
		carryIn = carryOut;
	}
	
	resultA.bitAND( chipSelect );	
}

void	aluMul( EWord & resultA, EWord & inA, EWord & inR, EBit & chipSelect )
{
    assert( resultA.size() == inA.size() );
    assert( resultA.size() == inR.size() );

    int     len = inA.size();
    //EWord   sum( inA.pk(), len, ProcessorHE::getZeroBias() );
    EWord & sum = resultA;  // inna nazwa na resultA, ale to to samo co resultA
    sum = inA;
    sum.bitAND( inR[0] );

    for( int i = 1 ; i < len; ++i )
    {
        EWord   tmp( inA.pk(), len, ProcessorHE::getZeroBias() );
        inA.copyTo( tmp, 0, i, len - i );
        tmp.bitAND( inR[i] );

        EBit carryIn = ProcessorHE::getZeroBias();
        EBit carryOut( inA.pk(), 0 );

        for (int j = 0 ; j < len ; ++j )
        {
            EBit bitTmp = sum[j];
            EBit::fulladd( sum[j], carryOut, bitTmp, tmp[j], carryIn );
            sum[j].recrypt();
            carryOut.recrypt();
            carryIn = carryOut;
        }

    }
    resultA.bitAND( chipSelect );
}


