#include "tofile.h"
#include <stdio.h>

void ToFile::wordToFile( EWord & word, char *filename )
{
	FILE *myFile;
    myFile = fopen (filename, "w+");

    word.toFile(myFile);

    fclose (myFile);
}

void ToFile::wordFromFile( EWord & word, char *filename)
{
	FILE *myFile;
    myFile = fopen (filename, "r+");

    word.fromFile(myFile);

    fclose (myFile);
}

bool ToFile::readMemory(std::vector<EWord> &out, const char *filename, fhe_pk_t &pk)
{
    FILE *memFile;
    memFile = fopen (filename, "r");
    int res;

    if( ! memFile )
    {
        printf("Error: fail to open file %s\n", filename );
        return false;
    }

    int wordsCount;
    int wordsSize;
    fread( (char*) &wordsCount, sizeof(int), 1 , memFile );
    fread( (char*) &wordsSize, sizeof(int), 1 , memFile );

    for( int i = 0 ; i < wordsCount ; ++i )
    {
        EWord word( pk, wordsSize );
        word.fromFile( memFile );
        out.push_back( word );
    }

    fclose( memFile );
}

bool ToFile::writeMemory(const char *filename, const std::vector<EWord> &in)
{
    FILE *memFile;
    memFile = fopen (filename, "w+");
    int res;

    if( ! memFile )
    {
        printf("Error: fail to open file %s\n", filename );
        return false;
    }

    int wordsCount  = in.size();

    if( in.size() == 0 )
    {
        fclose( memFile );
        return true;
    }

    int wordSize = in[0].size();
    fwrite( (char*) &wordsCount, sizeof( wordSize ), 1, memFile );
    fwrite( (char*) &wordSize, sizeof( wordSize ), 1, memFile );

    for( int i = 0 ;  i < wordsCount ; ++i )
    {
        EWord  word = in[i];
        word.toFile( memFile );
    }

    fclose( memFile );
}

bool ToFile::readPK( const char *filename, fhe_pk_t &outPK )
{
    FILE *pkFile;
    pkFile = fopen (filename, "r");
    int res;
    if( ! pkFile )
    {
        printf("Error: fail to open file %s\n", filename );
        return false;
    }

    //mpz_t p, alpha;
    //mpz_t c[S1], B[S1];

    fhe_pk_init( outPK );

    //mpz_init( outPK[0].p );
    //mpz_init( outPK[0].alpha );

    res = mpz_inp_raw ( outPK[0].p, pkFile );
    if (res == 0)
    {
        printf("Error: mpz_out_raw\n");
        fclose (pkFile);
        return false;
    }

    res = mpz_inp_raw ( outPK[0].alpha, pkFile );
    if (res == 0)
    {
        printf("Error: mpz_out_raw\n");
        fclose (pkFile);
        return false;
    }

    for( int i  = 0 ; i < S1 ; ++i  )
    {
        //mpz_init( outPK[0].c[i] );
        res = mpz_inp_raw ( outPK[0].c[i], pkFile );
        if (res == 0)
        {
            printf("Error: mpz_out_raw\n");
            fclose (pkFile);
            return false;
        }
    }

    for( int i  = 0 ; i < S1 ; ++i  )
    {
        //mpz_init( outPK[0].B[i]);
        res = mpz_inp_raw ( outPK[0].B[i], pkFile );
        if (res == 0)
        {
            printf("Error: mpz_out_raw\n");
            fclose (pkFile);
            return false;
        }
    }


    fclose (pkFile);
    return true;
}

bool ToFile::readSK(const char *filename, fhe_sk_t &outSK)
{
    FILE *skFile;
    int res;
    skFile = fopen (filename, "r");
    if( ! skFile )
    {
        printf("Error: fail to open file %s\n", filename );
        return false;
    }
    //mpz_t p, B;

    fhe_sk_init( outSK );
    //mpz_init( outSK[0].p );
    //mpz_init( outSK[0].B );

    res = mpz_inp_raw ( outSK[0].p, skFile );
    if (res == 0)
    {
        printf("Error: mpz_out_raw\n");
        fclose (skFile);
        return false;
    }

    res = mpz_inp_raw ( outSK[0].B, skFile );
    if (res == 0)
    {
        printf("Error: mpz_out_raw\n");
        fclose (skFile);
        return false;
    }

    fclose (skFile);
    return true;
}

bool ToFile::writePK(const char *filename, const fhe_pk_t &inPK)
{
    FILE *pkeyFile;
    pkeyFile = fopen (filename, "w+");
    if( ! pkeyFile )
    {
        printf("Error: fail to open file %s\n", filename );
        return false;
    }
    //mpz_t p, alpha;
    //mpz_t c[S1], B[S1];


    int res = mpz_out_raw (pkeyFile, inPK[0].p );
    if (res == 0)
    {
        printf("Error: mpz_out_raw\n");
        fclose (pkeyFile);
        return false;
    }

    res = mpz_out_raw (pkeyFile, inPK[0].alpha );
    if (res == 0)
    {
        printf("Error: mpz_out_raw\n");
        fclose (pkeyFile);
        return false;
    }

    for( int i  = 0 ; i < S1 ; ++i  )
    {
        res = mpz_out_raw (pkeyFile, inPK[0].c[i] );
        if (res == 0)
        {
            printf("Error: mpz_out_raw\n");
            fclose (pkeyFile);
            return false;
        }
    }

    for( int i  = 0 ; i < S1 ; ++i  )
    {
        res = mpz_out_raw (pkeyFile, inPK[0].B[i] );
        if (res == 0)
        {
            printf("Error: mpz_out_raw\n");
            fclose (pkeyFile);
            return false;
        }
    }

    fclose (pkeyFile);
    return true;
}

bool ToFile::writeSK(const char *filename, const fhe_sk_t &inSK)
{
    FILE *skeyFile;
    skeyFile = fopen (filename, "w+");
    if( ! skeyFile )
    {
        printf("Error: fail to open file %s\n", filename );
        return false;
    }
    //mpz_t p, B;

    int res = mpz_out_raw (skeyFile, inSK[0].p );
    if (res == 0)
    {
        printf("Error: mpz_out_raw\n");
        fclose (skeyFile);
        return false;
    }

    res = mpz_out_raw (skeyFile, inSK[0].B );
    if (res == 0)
    {
        printf("Error: mpz_out_raw\n");
        fclose (skeyFile);
        return false;
    }

    fclose (skeyFile);
    return true;

}
