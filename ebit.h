#ifndef EBIT_H
#define EBIT_H

#include "integer-fhe.h"
#include <iostream>
#include <fstream>

class EBit
{
    static const int cMaxDirtiness = 1;
    static const int cXORDirtinessComponent = 1;
    static const int cANDDirtinessComponent = 1;
    static const int cInitialDiritiness     = 1;

public:
    EBit( const fhe_pk_t pk, int initVal );
    EBit( const fhe_pk_t pk );
    EBit( const  EBit & e );
    ~EBit();

    EBit operator+( const EBit & e ) const;   //XOR
    EBit operator*( const EBit & e ) const;   //AND
    EBit operator~() const;

    EBit & operator =( const EBit & b );

    void    recrypt( );
    int     decrypt( fhe_sk_t sk );
    void    encrypt( int bitVal );
	
    void    print( fhe_sk_t sk );
    void    toFile (FILE * file);
    void    fromFile (FILE *file);
    
    static void fulladd( EBit & sum, EBit & cOut, EBit & a, EBit & b, EBit & cIn );

protected:
    void    _initialize();
    void    _deinitialize();
    void    _handleDirtiness( int additionalDirtiness );

private:
     int         m_dirtiness;
     mpz_t       m_encryption;
     fhe_pk_t    m_pk;

};

#endif // EBIT_H
