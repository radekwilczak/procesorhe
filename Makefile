NAME=processorhe
ENCODER=encoder
DECODER=decoder
CC       = g++
#CFLAGS   = -Wall -m64 -g3 -ggdb -std=c99 -I/opt/local/include -I../flint-1.6
#CPPFLAGS   = -Wall -m64 -g3 -ggdb  -I/opt/local/include -I../flint-1.6
#CPPFLAGS   = -g -Wall -m64 -stdlib=libstdc++ -I/opt/local/include -I../flint-1.6
CPPFLAGS   = -g -Wall -m64 -o3  -I/opt/local/include -I../flint-1.6
LDFLAGS += -lgmp -lflint -L/usr/local/lib -L../flint-1.6
SOURCE   = $(shell find * -name '*.cpp')
OBJECTS  = $(SOURCE:.cpp=.o)
PROCOBJS	= $(filter-out encoder.o decoder.o, $(OBJECTS))
ENCOBJS		= $(filter-out main.o decoder.o, $(OBJECTS))
DECOBJS		= $(filter-out main.o encoder.o, $(OBJECTS) )

.PHONY:    clean

all: $(NAME) $(ENCODER) $(DECODER)

run:
	./processorhe

$(DECODER): $(DECOBJS)
	$(CC) -o $(DECODER) $^ $(LDFLAGS)

$(ENCODER): $(ENCOBJS)
	$(CC) -o $(ENCODER) $^ $(LDFLAGS)

$(NAME): $(PROCOBJS)
	$(CC) -o $(NAME) $^ $(LDFLAGS)

clean:
	rm -f $(OBJECTS) $(NAME)

