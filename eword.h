#ifndef EWORD_H
#define EWORD_H

#include <vector>

#include "integer-fhe.h"
#include "ebit.h"

// reprezentacja bitowa Little endian.
// tzn. bit o najmniejszym indeksie jest najmniej znaczący ( EWord[0] ).
// [0] [1] [2]
//  1   1   0   == 3
//  0   1   1   == 6

class EWord
{
public:
    EWord( fhe_pk_t pk, int len, const EBit & initialBit );
    EWord( fhe_pk_t pk, int len, int initialValue );
    EWord( fhe_pk_t pk, int len );
    EWord( const EWord & word );

    EWord & operator =( const EWord & word );

    unsigned int size() const;
    //pobranie bitu na pozycji i
    EBit & operator[]( int i );
    const EBit & operator[]( int i ) const;

    void    copyTo( EWord & dest, int srcoffset = 0, int dstoffset = 0, int size = -1);

    void            encodeUInt( unsigned int value );
    unsigned int    decodeUInt( fhe_sk_t );

    void            encodeBits( const int bits[], int offset = 0, int size = -1 );
    void            decodeBits( int bits[], fhe_sk_t s, int offset = 0, int size = -1 );

    void                        encodeBits( const std::vector< int > & bits , int offset = 0, int size = -1 );
    const std::vector< int >    decodeBits( fhe_sk_t sk, int offset = 0, int size = -1 );

    void        bitAND( const EBit & bit );
    void        bitAND( EWord & result, const EBit & bit ) const;
    void        bitXOR( const EBit & bit );

    void        elementwiseXOR( EWord & result, const EWord & in ) const;
    void        elementwiseXOR( const EWord & in );

    void        negate();
    void        negated( EWord & negated ) const;

    EBit        ANDnorm() const;
    EBit        ANDnorm( unsigned int offset, unsigned int size ) const ;

    EBit        XORnorm();

    fhe_pk_t &  pk() { return m_pk; }
    void        print( fhe_sk_t sk );

    void        toFile(FILE *file);
    void        fromFile(FILE *file);

private:
    std::vector< EBit >         m_bits;
    unsigned int                m_N;
    fhe_pk_t                    m_pk;
};

#endif // EWORD_H
