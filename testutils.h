#ifndef TESTUTILS_H
#define TESTUTILS_H

#include "eword.h"

class TestUtils
{
public:
    static bool checkWord( fhe_sk_t sk, EWord & w, int bits[], int offset = 0, int size = -1);
    static bool checkWord( fhe_sk_t sk, EWord & w, unsigned int value );
};

#endif // TESTUTILS_H
