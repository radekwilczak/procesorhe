#ifndef MISCFUNCTIONS_H
#define MISCFUNCTIONS_H

#include "ebit.h"
#include "eword.h"

//MUX
// in - binarnie zapisany indeks bitu, ktory w out zostanie ustawiony na 1
// pozostale bity w out beda rowne 0
// np.  in  = 1 1 0  (7 dec)
//      out = 0 0 0 1 0 0 0
// np.  in  = 1 0 0  (1 dec)
//      out = 0 1 0 0 0 0 0
// np.  in  = 0 0 0  (1 dec)
//      out = 1 0 0 0 0 0 0
void    miscMUX( EWord & out, EWord & in, int addressBits = -1 );

//Jesli in to liczba binarna
// to out bedzie liczba binarna o jeden wieksza
void    miscINC( EWord & out, EWord & in, EBit & chipSelect );

//miscShiftN
// dowolne przesuniecie in i zapisanie tego do out
void    miscShiftN( EWord & out, EWord & in, int nPos );

//miscAdd
// dodaje dwa wektory a+b i zapisuje wynik w out
// przeniesienie trafia do carry
void    miscAdd( EWord & out, EBit & carry, EWord & a, EWord & b );

//sprawdza czy ktorys z bitow o danych indeksach jest 1.
EBit    miscAnyBitOne( EWord & in, int indexes[], unsigned int indexesCount );

#endif // MISCFUNCTIONS_H
