#include "controlfunctions.h"

void ctrlEq( EWord & resultF, EWord & inA, EWord & inR, EWord & inF, EBit & chipSelect )
{
    assert( resultF.size() == inA.size() );
    assert( resultF.size() == inR.size() );
    assert( resultF.size() == inF.size() );

    //linie tymczasowe
    EWord tmp( inA.pk(), resultF.size(), 0 );

    //xoruje A i R, jesli sa rowne powinny wyjsc same 0
    inA.elementwiseXOR( tmp, inR );

    //neguje wynik, Jesli poprzednio byly same 0 to teraz beda same 1
    tmp.negate();

    // ANDuje wszystkie bity ze sobą. Jesli byly to same 1 to wynikiem bedzie bit 1
    EBit Fbit = tmp.ANDnorm();

    // pierwszy bit zalezy od wyniku porownania
    resultF[ 0 ] = Fbit;

    // pozostale bity bez zmian
    inF.copyTo( resultF, 1, 1, inF.size() - 1 );

    //  ANDuje z linia wyboru
    //  zwracam zera, jesli ten modul nie zostal wybrany
    resultF.bitAND( chipSelect );
}

void ctrlJmp( EWord & resultCNT, EWord &inCNT, EWord & inF, EWord & op, EBit & chipSelect )
{
    assert( inCNT.size() >= resultCNT.size() );

    int cntLen = inCNT.size();
    EWord tmpOp( op.pk(), cntLen, 0 );
    op.copyTo( tmpOp, 0, 0, op.size() );    // kopia op, ktora ma te sama dlugosc do CNT

    EBit notF = ~inF[0];
    for( int i = 0 ; i < cntLen ; ++i)
    {
        resultCNT[ i ] = /*(inCNT[ i ] * notF ) +*/ (tmpOp[ i ] * inF[0]); // rozumiem że mogę korzystać tutaj z operatorów z klasy EBit?
    }

    resultCNT.bitAND( chipSelect );
}

void 	ctrlMove( EWord & to, EWord & from, EBit & chipSelect )
{
    //szerokosc wej. musi byc rowna szerokosci wyj.
    assert( from.size() == to.size() );

    for( int i = 0 ; i < from.size() ; ++i )
    {
        to[i] = from[i];
    }

    //jesli uklad nie jest 'wybrany' to wyjscie jest zerem.
    to.bitAND( chipSelect );
}

void    ctrlOp( EWord & resultR, EWord & op, EBit & chipSelect )
{
    assert( resultR.size() >= op.size() );

    for( int i = 0 ; i < op.size() ; ++i )
    {
        resultR[ i ] = op[ i ];
    }

    resultR.bitAND( chipSelect );
}


void	ctrlSwpf( EWord & resultA, EWord & resultF, EWord & inA, EWord & inF, EBit & chipSelect  )
{
    // wszystkie 4 rejestry muszą mieć taki sam rozmiar
    assert( inA.size() == inF.size() );
    assert( resultA.size() == resultF.size() );
    assert( resultA.size() == inA.size() );

    for ( int i = 0 ; i < resultA.size() ; ++i )
    {
        resultA[i] = inF[i];
        resultF[i] = inA[i];
    }

    resultA.bitAND( chipSelect );
    resultF.bitAND( chipSelect );
}

void ctrlGr( EWord & resultF, EWord & inA, EWord & inR, EWord & inF, EBit & chipSelect )
{
    assert( resultF.size() == inA.size() );
    assert( resultF.size() == inR.size() );
    assert( resultF.size() == inF.size() );

    int len = resultF.size();
    inF.copyTo( resultF );
    EWord   tmp( resultF.pk(), len );
    EBit    carry( resultF.pk(), 1 );
    EBit    gr( resultF.pk());
    for( int i = len -1 ; i >= 0; --i )
    {
        gr          = (~inA[i]) * inR[i];
        //W = C~AB
        tmp[ i ]    = carry * gr;
        // przeniesienie do nast. bloku
        //C = ~(A xor B) xor (~AB)
        carry = (~(inA[i]+inR[i]) + gr)*carry;
    }
    tmp.negate();

    resultF[0]  = ~ tmp.ANDnorm();
    resultF.bitAND( chipSelect );
}
