#include "tests.h"
#include "controlfunctions.h"
#include "alufunctions.h"
#include "processor.h"
#include "tofile.h"


#include <math.h>

Tests::Tests()
{
}



void Tests::poligoon( bool flag )
{
    const int codeMemSize   = 10;
    const int dataMemSize   = 10;
    const int operandSize   = 2;
    const int idSize        = 4;
    const int instrSize     = operandSize + idSize;
    const int wordSize      = 4;

    ProcessorHE proc;

    std::vector< EWord > dataMemory;
    std::vector< EWord > codeMemory;

    //fhe_pk_init(pk);
    //fhe_sk_init(sk);

    //fhe_keygen(pk, sk);
    ToFile::readPK( "pk.pk", pk );
    ToFile::readSK( "sk.sk", sk );

    ToFile::readMemory( codeMemory, "code.mem", pk );
    ToFile::readMemory( dataMemory, "data.mem", pk );

    proc.initialize( pk );
    proc.setSK( sk );
    proc.setParams( wordSize, operandSize, idSize );
    proc.setDebugMode( flag );

//    for( int i = 0 ; i < codeMemSize ; ++i )
//    {
//        EWord temp( pk, instrSize, 0 );
//        temp.encodeUInt( i );
//        codeMemory.push_back( temp );
//    }



    {
        EWord temp( pk, instrSize, 0 );

//        temp.encodeUInt( 12 | (2 << 4 ) );
//        codeMemory.push_back( temp );

//        temp.encodeUInt( 0 );
//        codeMemory.push_back( temp );

//        temp.encodeUInt( 10 );
//        codeMemory.push_back( temp );

//        temp.encodeUInt( 12 | (3 << 4 ) );
//        codeMemory.push_back( temp );

//        temp.encodeUInt( 7 );
//        codeMemory.push_back( temp );

//        temp.encodeUInt( 8 );
//        codeMemory.push_back( temp );

//        temp.encodeUInt( 12 | (2 << 4 ) );
//        codeMemory.push_back( temp );

//        temp.encodeUInt( 10 );
//        codeMemory.push_back( temp );

//        temp.encodeUInt( 5 );
//        codeMemory.push_back( temp );

//        temp.encodeUInt( 15 | ( 1 << 4 )  );
//        codeMemory.push_back( temp );

    }

    for( int i = 0 ; i < dataMemSize ; ++i )
    {
        EWord temp( pk, wordSize, 0 );
        //temp.encodeUInt( i );
        //dataMemory.push_back( temp );
    }

    proc.loadDataMemory( dataMemory );
    proc.loadCodeMemory( codeMemory );

    proc.run();

    ToFile::writeMemory("resultData.mem", proc.getDataMemory() );

    fhe_sk_clear( sk );
    fhe_pk_clear( pk );

    processor.deinitialize();
}

void Tests::dirtTest()
{
    initTests();

    mpz_t e,zero,one;
    mpz_t cpy;

    mpz_init( e );
    mpz_init( cpy );
    mpz_init( zero );
    mpz_init( one );

    fhe_encrypt( e, pk, 1 );
    fhe_encrypt( zero, pk, 0 );
    fhe_encrypt( one, pk, 1 );

    for( int i = 0 ; i < 100000 ; ++i )
    {
        //mpz_add( e, e, one );
        //mpz_mod( e, e, pk->p );
        //mpz_set( cpy, e );
//        int m = fhe_decrypt( cpy, sk );
//        if( i % 2 )
//        {
//            assert( m == 1 );
//        }
//        else
//        {
//            assert( m == 0 );
//        }

        mpz_add( e, e, zero );
        mpz_mod( e, e, pk->p );

        mpz_mul( e, e, one );
        mpz_mod( e, e, pk->p );
        mpz_set( cpy, e );
        int m = fhe_decrypt( cpy, sk );
        assert( m == 1 );

    }

    mpz_clear( e );
    mpz_clear( cpy );
    mpz_clear( zero );
    mpz_clear( one );

    deinitTests();
}

void Tests::dirtTest2()
{
    initTests();

    EBit    e( pk, 1 );
    EBit    one( pk, 1 );
    EBit    zero( pk, 0 );

    for( int i = 0 ; i < 1000 ; ++i )
    {
        e = e * one;
        e = e + zero;
        int m = e.decrypt( sk );
        assert( m == 1 );
    }


    deinitTests();
}

void Tests::tests()
{
    initTests();

    test_EBit_enc_dec();
    test_EBit_copy_assign();
    test_EBit_and_xor();

    test_EWord_enc_dec();
    test_EWord_copy();
    test_EWord_encodeDecodeInt();

    test_CtrlEq();
    test_ctrlJmp();
    test_ctrlGr();

	test_aluAdd();
    test_aluMul();

    test_MiscShiftN();
    test_MiscMUX();


    test_memRead();
    test_memWrite();

    test_toFile();

    deinitTests();
}


void Tests::initTests()
{
    fhe_pk_init(pk);
    fhe_sk_init(sk);

    fhe_keygen(pk, sk);

    processor.initialize( pk );
}

void Tests::deinitTests()
{
    fhe_sk_clear( sk );
    fhe_pk_clear( pk );

    processor.deinitialize();
}

void Tests::test_EBit_enc_dec()
{
    {
        EBit bit0( pk, 0 );
        EBit bit1( pk, 1 );

        assert( bit0.decrypt( sk ) == 0 );
        assert( bit1.decrypt( sk ) == 1 );
    }



    {
        EBit bit0( pk, 1 );
        EBit bit1( pk, 0 );

        bit0.encrypt( 0 );
        bit1.encrypt( 1 );

        assert( bit0.decrypt( sk ) == 0 );
        assert( bit1.decrypt( sk ) == 1 );
    }

}

void Tests::test_EBit_copy_assign()
{
    {
        EBit bit0( pk, 0 );
        EBit bit1( pk, 1 );

        EBit bit0c( bit0 );
        EBit bit1c( bit1 );

        assert( bit0c.decrypt( sk ) == 0 );
        assert( bit1c.decrypt( sk ) == 1 );
    }

    {
        EBit bit0( pk, 0 );
        EBit bit1( pk, 1 );

        EBit bit0c( pk, 1);
        EBit bit1c( pk, 0);

        bit0c = bit0;
        bit1c = bit1;

        assert( bit0c.decrypt( sk ) == 0 );
        assert( bit1c.decrypt( sk ) == 1 );
    }
}

void Tests::test_EBit_and_xor()
{
    {
        EBit bit0_a( pk, 0 );
        EBit bit0_b( pk, 0 );
        EBit bit1_a( pk, 1 );
        EBit bit1_b( pk, 1 );

        EBit bit0and1_a = bit0_a * bit1_a;

        EBit bit1and1_a = bit1_a * bit1_a;
        EBit bit1and1_b = bit1_a * bit1_b;

        EBit bit0and0_a = bit0_a * bit0_a;
        EBit bit0and0_b = bit0_a * bit0_b;

        assert( bit0and1_a.decrypt( sk ) == 0 );
        assert( bit1and1_a.decrypt( sk ) == 1 );
        assert( bit1and1_b.decrypt( sk ) == 1 );
        assert( bit0and0_a.decrypt( sk ) == 0 );
        assert( bit0and0_b.decrypt( sk ) == 0 );

        assert( bit0_a.decrypt(sk) == 0 );
        assert( bit0_b.decrypt(sk) == 0 );
        assert( bit1_a.decrypt(sk) == 1 );
        assert( bit1_b.decrypt(sk) == 1 );

    }

    {
        EBit bit0_a( pk, 0 );
        EBit bit0_b( pk, 0 );
        EBit bit1_a( pk, 1 );
        EBit bit1_b( pk, 1 );

        EBit bit0xor1_a = bit0_a + bit1_a;

        EBit bit1xor1_a = bit1_a + bit1_a;
        EBit bit1xor1_b = bit1_a + bit1_b;

        EBit bit0xor0_a = bit0_a + bit0_a;
        EBit bit0xor0_b = bit0_a + bit0_b;

        assert( bit0xor1_a.decrypt( sk ) == 1 );
        assert( bit1xor1_a.decrypt( sk ) == 0 );
        assert( bit1xor1_b.decrypt( sk ) == 0 );
        assert( bit0xor0_a.decrypt( sk ) == 0 );
        assert( bit0xor0_b.decrypt( sk ) == 0 );

        assert( bit0_a.decrypt(sk) == 0 );
        assert( bit0_b.decrypt(sk) == 0 );
        assert( bit1_a.decrypt(sk) == 1 );
        assert( bit1_b.decrypt(sk) == 1 );
    }
}



void Tests::test_EWord_enc_dec()
{
    const int len = 8;
    EWord   word0( pk, len, 0 );
    EWord   word1( pk, len, 1 );

    for( int i = 0 ; i < len ; ++i  )
    {
        assert( word0[i].decrypt( sk ) == 0 );
        assert( word1[i].decrypt( sk ) == 1 );
    }
}


void Tests::test_EWord_copy()
{
    {
        const int len_a = 8;
        const int len_b = 8;

        EWord   word_a( pk, len_a, 0 );
        EWord   word_b( pk, len_b, 1 );

        word_a.copyTo( word_b );

        for( int i = 0 ;i < len_a ; ++i )
        {
            assert( word_a[i].decrypt( sk ) == 0 );
        }

        for( int i = 0 ;i < len_b ; ++i )
        {
            assert( word_b[i].decrypt( sk ) == 0 );
        }
    }
}

void Tests::test_EWord_encodeDecodeInt()
{
    {
        const int len_a = 8;
        EWord   word( pk, len_a, 0 );

        word.encodeUInt( 255 );
        unsigned int res = word.decodeUInt( sk );
        assert( res == 255 );
    }

    {
        const int len_a = 8;
        EWord   word( pk, len_a, 0 );

        word.encodeUInt( 173 );
        unsigned int res = word.decodeUInt( sk );
        assert( res == 173 );
    }

}

void Tests::test_aluAdd()
{
	const int len = 5;

    // A = 10, R = 21 i chipSelect = 1
    {
        EWord   A( pk, len, 0 );
        EWord   R( pk, len, 0 );
        EWord   resultA( pk, len, 0 );
        EBit    chipSelect( pk, 1 );

        A.encodeUInt( 10 );
        R.encodeUInt( 21 );

        aluAdd( resultA, A, R, chipSelect );
    
        int expectedFBits[ len ] = { 1, 1, 1, 1, 1 };

        assert( TestUtils::checkWord( sk, resultA, expectedFBits ) == true );
    }
    
   	// A = 17, R = 17 i chipSelect = 1
    {
        EWord   A( pk, len, 0 );
        EWord   R( pk, len, 0 );
        EWord   resultA( pk, len, 0 );
        EBit    chipSelect( pk, 1 );

        A.encodeUInt( 17 );
        R.encodeUInt( 17 );

        aluAdd( resultA, A, R, chipSelect );
    
        int expectedFBits[ len ] = { 0, 1, 0, 0, 0 };

        assert( TestUtils::checkWord( sk, resultA, expectedFBits ) == true );
    }
    
	// A = 31, R = 31 i chipSelect = 1
    {
        EWord   A( pk, len, 0 );
        EWord   R( pk, len, 0 );
        EWord   resultA( pk, len, 0 );
        EBit    chipSelect( pk, 1 );

        A.encodeUInt( 31 );
        R.encodeUInt( 31 );

        aluAdd( resultA, A, R, chipSelect );
    
        int expectedFBits[ len ] = { 0, 1, 1, 1, 1 };

        assert( TestUtils::checkWord( sk, resultA, expectedFBits ) == true );
    }
}

void Tests::test_aluMul()
{
    const int len = 4;


//    {
//        EWord   inA( pk, len, 0 );
//        EWord   inR( pk, len, 0 );
//        EWord   resultA( pk, len, 0 );

//        inA.encodeUInt( 2 );
//        inR.encodeUInt( 3 );

//        int     len = inA.size();
//        //EWord   sum( inA.pk(), len, ProcessorHE::getZeroBias() );
//        EWord & sum = resultA;  // inna nazwa na resultA, ale to to samo co resultA
//        sum = inA;
//        sum.bitAND( inR[0] );

//        sum.print( sk );
//        for( int i = 1 ; i < len; ++i )
//        {
//            EWord   tmp( inA.pk(), len, 0 );
//            inA.copyTo( tmp, 0, i, len - i );
//            tmp.bitAND( inR[i] );
//            tmp.print( sk );
//            EBit carryIn = ProcessorHE::getZeroBias();
//            EBit carryOut = ProcessorHE::getZeroBias();

//            for (int j = 0 ; j < len ; ++j )
//            {
//                EBit bt = sum[j];
//                EBit::fulladd( sum[j], carryOut, sum[j], bt, carryIn );
//                sum[j].recrypt();
//                carryOut.recrypt();
//                carryIn = carryOut;
//            }
//            sum.print( sk );
//        }

//    }

    // A = 2, R = 3 i chipSelect = 1
    // wynik = 6
    {
        EWord   A( pk, len, 0 );
        EWord   R( pk, len, 0 );
        EWord   resultA( pk, len, 0 );
        EBit    chipSelect( pk, 1 );

        A.encodeUInt( 2 );
        R.encodeUInt( 3 );

        aluMul( resultA, A, R, chipSelect );

        resultA.print(sk);

        int expectedFBits[ len ] = { 0, 1, 1, 0 };

        assert( TestUtils::checkWord( sk, resultA, expectedFBits ) == true );
    }

    // A = 4, R = 5 i chipSelect = 1
    // wynik = 20 mod 16 = 4
    {
        EWord   A( pk, len, 0 );
        EWord   R( pk, len, 0 );
        EWord   resultA( pk, len, 0 );
        EBit    chipSelect( pk, 1 );

        A.encodeUInt( 4 );
        R.encodeUInt( 5 );

        aluMul( resultA, A, R, chipSelect );

        resultA.print(sk);

        int expectedFBits[ len ] = { 0, 0, 1, 0 };

        assert( TestUtils::checkWord( sk, resultA, expectedFBits ) == true );
    }

    // A = 0, R = 0 i chipSelect = 1
    // wynik = 0
    {
        EWord   A( pk, len, 0 );
        EWord   R( pk, len, 0 );
        EWord   resultA( pk, len, 0 );
        EBit    chipSelect( pk, 1 );

        A.encodeUInt( 0 );
        R.encodeUInt( 0 );

        aluMul( resultA, A, R, chipSelect );

        resultA.print(sk);

        int expectedFBits[ len ] = { 0, 0, 0, 0 };

        assert( TestUtils::checkWord( sk, resultA, expectedFBits ) == true );
    }

}

void Tests::test_CtrlEq()
{
    const int len = 5;


    // A rowna sie R i chipSelect = 1
    {
        EWord   A( pk, len, 0 );
        EWord   R( pk, len, 0 );
        EWord   F( pk, len, 0 );
        EWord   resultF( pk, len, 0 );
        EBit    chipSelect( pk, 1 );

        A.encodeUInt( 7 );
        R.encodeUInt( 7 );
        A.print( sk );
        R.print( sk );

        //linie tymczasowe
        EWord tmp( A.pk(), resultF.size(), 0 );

        //xoruje A i R, jesli sa rowne powinny wyjsc same 0
        A.elementwiseXOR( tmp, R );
        tmp.print( sk );


        //neguje wynik, Jesli poprzednio byly same 0 to teraz beda same 1
        tmp.negate();
        tmp.print( sk );

        // ANDuje wszystkie bity ze sobą. Jesli byly to same 1 to wynikiem bedzie bit 1
        EBit Fbit = tmp.ANDnorm();
        Fbit.print( sk );

        // pierwszy bit zalezy od wyniku porownania
        resultF[ 0 ] = Fbit;

        // pozostale bity bez zmian
        F.copyTo( resultF, 1, 1, F.size() - 1 );
        resultF.print( sk );

        //  ANDuje z linia wyboru
        //  zwracam zera, jesli ten modul nie zostal wybrany
        resultF.bitAND( chipSelect );
    }


    // A rowna sie R i chipSelect = 1
    {
        EWord   A( pk, len, 0 );
        EWord   R( pk, len, 0 );
        EWord   F( pk, len, 0 );
        EWord   resultF( pk, len, 0 );
        EBit    chipSelect( pk, 1 );

        A.encodeUInt( 17 );
        R.encodeUInt( 17 );

        ctrlEq( resultF, A, R, F, chipSelect );

        int expectedFBits[ len ] = { 1, 0, 0, 0, 0 };

        assert( TestUtils::checkWord( sk, resultF, expectedFBits ) == true );
    }

    // A rozne R i chipSelect = 1
    {
        EWord   A( pk, len, 0 );
        EWord   R( pk, len, 0 );
        EWord   F( pk, len, 0 );
        EWord   resultF( pk, len, 0 );
        EBit    chipSelect( pk, 1 );

        A.encodeUInt( 1 );
        R.encodeUInt( 0 );

        ctrlEq( resultF, A, R, F, chipSelect );

        int expectedFBits[ len ] = { 0, 0, 0, 0, 0 };

        assert( TestUtils::checkWord( sk, resultF, expectedFBits ) == true );
    }

    // A rowna sie R ale chipSelect = 0
    {
        EWord   A( pk, len, 0 );
        EWord   R( pk, len, 0 );
        EWord   F( pk, len, 0 );
        EWord   resultF( pk, len, 0 );
        EBit    chipSelect( pk, 0 );

        A.encodeUInt( 31 );
        R.encodeUInt( 31 );

        ctrlEq( resultF, A, R, F, chipSelect );

        int expectedFBits[ len ] = { 0, 0, 0, 0, 0 };

        assert( TestUtils::checkWord( sk, resultF, expectedFBits ) == true );
    }

}

void Tests::test_ctrlJmp()
{
    const int len = 5;


    // F[0] == 0
    {
        EWord   inCNT( pk, len, 0 );
        EWord   inF( pk, len, 0 );
        EWord   op( pk, len, 0 );
        EWord   resultCNT( pk, len, 0 );
        EBit    chipSelect( pk, 1 );

        inCNT.encodeUInt( 31 );
        inF.encodeUInt( 0 );
        op.encodeUInt( 2 );

        inF.print(sk);
        inCNT.print(sk);
        op.print(sk);

        ctrlJmp( resultCNT, inCNT, inF, op, chipSelect );

        resultCNT.print(sk);

        int expectedFBits[ len ] = { 1, 1, 1, 1, 1 };

        printf("test_ctrlJmp\n");

        assert( TestUtils::checkWord( sk, resultCNT, expectedFBits ) == true );
    }   

    // F[0] == 1
    {
        EWord   inCNT( pk, len, 0 );
        EWord   inF( pk, len, 0 );
        EWord   op( pk, len, 0 );
        EWord   resultCNT( pk, len, 0 );
        EBit    chipSelect( pk, 1 );

        inCNT.encodeUInt( 31 );
        inF.encodeUInt( 1 );
        op.encodeUInt( 2 );

        inF.print(sk);
        inCNT.print(sk);
        op.print(sk);

        ctrlJmp( resultCNT, inCNT, inF, op, chipSelect );

        resultCNT.print(sk);

        int expectedFBits[ len ] = { 0, 1, 0, 0, 0 };

        printf("test_ctrlJmp\n");

        assert( TestUtils::checkWord( sk, resultCNT, expectedFBits ) == true );
    }

    // F[0] == 1
    // chipSelect = 0
    {
        EWord   inCNT( pk, len, 0 );
        EWord   inF( pk, len, 0 );
        EWord   op( pk, len, 0 );
        EWord   resultCNT( pk, len, 0 );
        EBit    chipSelect( pk, 0 );

        inCNT.encodeUInt( 31 );
        inF.encodeUInt( 1 );
        op.encodeUInt( 2 );

        inF.print(sk);
        inCNT.print(sk);
        op.print(sk);

        ctrlJmp( resultCNT, inCNT, inF, op, chipSelect );

        resultCNT.print(sk);

        int expectedFBits[ len ] = { 0, 0, 0, 0, 0 };

        printf("test_ctrlJmp\n");

        assert( TestUtils::checkWord( sk, resultCNT, expectedFBits ) == true );
    }
}

void Tests::test_ctrlGr()
{
    const int len = 5;

    // F[0] == 1
    // chipSelect = 1
    {
        EWord   inA( pk, len, 0 );
        EWord   inR( pk, len, 0 );
        EWord   inF( pk, len, 0 );
        EWord   resultF( pk, len, 0 );
        EBit    chipSelect( pk, 1 );

        inA.encodeUInt( 3 );
        inR.encodeUInt( 7 );

        ctrlGr( resultF, inA, inR, inF, chipSelect );
        resultF.print(sk);

        int expectedFBits[ len ] = { 1, 0, 0, 0, 0 };
        assert( TestUtils::checkWord( sk, resultF, expectedFBits ) == true );
    }
    // F[0] == 0
    // chipSelect = 1
    {
        EWord   inA( pk, len, 0 );
        EWord   inR( pk, len, 0 );
        EWord   inF( pk, len, 0 );
        EWord   resultF( pk, len, 0 );
        EBit    chipSelect( pk, 1 );

        inA.encodeUInt( 10 );
        inR.encodeUInt( 6 );

        ctrlGr( resultF, inA, inR, inF, chipSelect );
        resultF.print(sk);

        int expectedFBits[ len ] = { 0, 0, 0, 0, 0 };
        assert( TestUtils::checkWord( sk, resultF, expectedFBits ) == true );
    }
    // F[0] == 1
    // chipSelect = 0
    {
        EWord   inA( pk, len, 0 );
        EWord   inR( pk, len, 0 );
        EWord   inF( pk, len, 0 );
        EWord   resultF( pk, len, 0 );
        EBit    chipSelect( pk, 0 );

        inA.encodeUInt( 3 );
        inR.encodeUInt( 7 );

        ctrlGr( resultF, inA, inR, inF, chipSelect );

        int expectedFBits[ len ] = { 0, 0, 0, 0, 0 };
        assert( TestUtils::checkWord( sk, resultF, expectedFBits ) == true );
    }
}

void Tests::test_MiscINC()
{
    //TODO:
}

void Tests::test_MiscMUX()
{
    {
        const int addrlen = 3;
        const int outlen = pow( 2, addrlen );

        EWord out( pk, outlen, 0 );
        EWord addr( pk, addrlen, 0 );


        addr.print( sk );
        miscMUX( out, addr );
        out.print( sk );

        int expectedFBits[ outlen ];
        memset( expectedFBits, 0, outlen*sizeof( int ));
        expectedFBits[ 0 ]  = 1;

        assert( TestUtils::checkWord( sk, out, expectedFBits ) == true );
    }

    {
        const int addrlen = 3;
        const int outlen = pow( 2, addrlen );

        EWord out( pk, outlen, 0 );
        EWord addr( pk, addrlen, 0 );

        addr.encodeUInt( 1 );

        addr.print( sk );
        miscMUX( out, addr );
        out.print( sk );

        int expectedFBits[ outlen ];
        memset( expectedFBits, 0, outlen*sizeof( int ));
        expectedFBits[ 1 ]  = 1;

        assert( TestUtils::checkWord( sk, out, expectedFBits ) == true );
    }


    {
        const int addrlen = 3;
        const int outlen = pow( 2, addrlen );

        EWord out( pk, outlen, 0 );
        EWord addr( pk, addrlen, 0 );

        addr.encodeUInt( 7 );

        addr.print( sk );
        miscMUX( out, addr );
        out.print( sk );

        int expectedFBits[ outlen ];
        memset( expectedFBits, 0, outlen*sizeof( int ));
        expectedFBits[ 7 ]  = 1;

        assert( TestUtils::checkWord( sk, out, expectedFBits ) == true );
    }

    {
        const int addrlen = 3;
        const int outlen = pow( 2, addrlen );

        EWord out( pk, outlen, 0 );
        EWord addr( pk, addrlen, 0 );

        addr.encodeUInt( 3 );

        addr.print( sk );
        miscMUX( out, addr );
        out.print( sk );

        int expectedFBits[ outlen ];
        memset( expectedFBits, 0, outlen*sizeof( int ));
        expectedFBits[ 3 ]  = 1;

        assert( TestUtils::checkWord( sk, out, expectedFBits ) == true );
    }

}

void Tests::test_MiscShiftN()
{
    {
        const int len = 5;
        EWord out( pk, len, 0 );
        EWord in( pk, len, 1 );

        miscShiftN( out, in, 2 );

        int expectedFBits[ len ] = { 0, 0, 1, 1, 1 };

        assert( TestUtils::checkWord( sk, out, expectedFBits ) == true );
    }


    {
        const int len = 5;
        EWord out( pk, len, 0 );
        EWord in( pk, len, 1 );

        miscShiftN( out, in, -2 );

        int expectedFBits[ len ] = { 1, 1, 1, 0, 0 };

        assert( TestUtils::checkWord( sk, out, expectedFBits ) == true );
    }
}

void Tests::test_memRead()
{
    const int memSize       = 8;
    const int memWordSize   = 5;
    {
        std::vector< EWord >    memory;
        EBit                    chipSelect( pk, 1 );

        for( int i = 0 ; i < memSize ; ++i )
        {
            EWord tmp( pk, memWordSize, 0 );
            tmp.encodeUInt( i );
            memory.push_back( tmp );
        }

        EWord out( pk, memWordSize, 1 );
        EWord select( pk, memSize, 0 );
        select[ 0 ].encrypt( 1 );

        memRead( out, memory, select, chipSelect);

        int expectedFBits[ memWordSize ] = { 0, 0, 0, 0, 0 };
        assert( TestUtils::checkWord( sk, out, expectedFBits ) == true );
    }

    {
        std::vector< EWord >    memory;
        EBit                    chipSelect( pk, 1 );

        for( int i = 0 ; i < memSize ; ++i )
        {
            EWord tmp( pk, memWordSize, 0 );
            tmp.encodeUInt( i );
            memory.push_back( tmp );
        }

        EWord out( pk, memWordSize, 1 );
        EWord select( pk, memSize, 0 );
        select[ 3 ].encrypt( 1 );

        memRead( out, memory, select, chipSelect);

        int expectedFBits[ memWordSize ] = { 1, 1, 0, 0, 0 };
        assert( TestUtils::checkWord( sk, out, expectedFBits ) == true );
    }

    {
        std::vector< EWord >    memory;
        EBit                    chipSelect( pk, 1 );

        for( int i = 0 ; i < memSize ; ++i )
        {
            EWord tmp( pk, memWordSize, 0 );
            tmp.encodeUInt( i );
            memory.push_back( tmp );
        }

        for( int i = 0 ; i < memSize ; ++i )
        {
            EWord out( pk, memWordSize, 1 );
            EWord select( pk, memSize, 0 );
            select[ i ].encrypt( 1 );

            memRead( out, memory, select, chipSelect);

            assert( TestUtils::checkWord( sk, out, i ) == true );
        }
    }

    {
        std::vector< EWord >    memory;
        EBit                    chipSelect( pk, 0 );

        for( int i = 0 ; i < memSize ; ++i )
        {
            EWord tmp( pk, memWordSize, 0 );
            tmp.encodeUInt( i );
            memory.push_back( tmp );
        }

        for( int i = 0 ; i < memSize ; ++i )
        {
            EWord out( pk, memWordSize, 1 );
            EWord select( pk, memSize, 0 );
            select[ i ].encrypt( 1 );

            memRead( out, memory, select, chipSelect);

            int expectedFBits[ memWordSize ] = { 0, 0, 0, 0, 0 };
            assert( TestUtils::checkWord( sk, out, expectedFBits ) == true );
        }
    }

}

void Tests::test_memWrite()
{
    const int memSize       = 8;
    const int memWordSize   = 5;
    {
        std::vector< EWord >    memory;
        EBit                    chipSelect( pk, 1 );

        for( int i = 0 ; i < memSize ; ++i )
        {
            EWord tmp( pk, memWordSize, 0 );
            tmp.encodeUInt( i );
            memory.push_back( tmp );
        }

        EWord in( pk, memWordSize, 1 );
        EWord select( pk, memSize, 0 );
        select[ 3 ].encrypt( 1 );

        memWrite( memory, in, select, chipSelect );

        for( int i = 0 ; i < memSize ; ++i )
        {
            if( i == 3 )
            {
                int expectedFBits[ memWordSize ] = { 1, 1, 1, 1, 1 };
                assert( TestUtils::checkWord( sk, memory[i], expectedFBits ) == true );
            }
            else
            {
                assert( TestUtils::checkWord( sk, memory[i], i ) == true );
            }
        }
    }

    {
        std::vector< EWord >    memory;
        EBit                    chipSelect( pk, 0 );

        for( int i = 0 ; i < memSize ; ++i )
        {
            EWord tmp( pk, memWordSize, 0 );
            tmp.encodeUInt( i );
            memory.push_back( tmp );
        }

        EWord in( pk, memWordSize, 1 );
        EWord select( pk, memSize, 0 );
        select[ 3 ].encrypt( 1 );

        memWrite( memory, in, select, chipSelect );

        for( int i = 0 ; i < memSize ; ++i )
        {
            assert( TestUtils::checkWord( sk, memory[i], i ) == true );
        }
    }
}

void Tests::test_toFile()
{
    char fileName[] = "plik.bin";

    {
        EWord word1( pk, 4, 0 );
        word1.encodeUInt( 7 );

        ToFile::wordToFile( word1, fileName );

        EWord word2( pk, 2, 0 );
        word2.encodeUInt( 1 );

        ToFile::wordFromFile( word2, fileName);

        int expectedFBits[ 4 ] = { 1, 1, 1, 0 };

        assert( TestUtils::checkWord( sk, word2, expectedFBits ) == true );
    }

    {
        EWord word1( pk, 4, 0 );
        word1.encodeUInt( 2 );

        ToFile::wordToFile( word1, fileName );

        EWord word2( pk, 4, 0 );
        word2.encodeUInt( 7 );

        ToFile::wordFromFile( word2, fileName);

        int expectedFBits[ 4 ] = { 0, 1, 0, 0 };

        assert( TestUtils::checkWord( sk, word2, expectedFBits ) == true );
    }
}
