#include "ebit.h"
#include <algorithm>
#include "processor.h"


EBit::EBit( const fhe_pk_t pk, int initVal)
{
    _initialize();
    m_pk[0] = pk[0];

    encrypt( initVal );
}

EBit::EBit(const fhe_pk_t pk)
{
    _initialize();
    m_pk[0] = pk[0];
}

EBit::EBit(const EBit &e)
{
    _initialize();
    *this = e;
}

EBit::~EBit()
{
    _deinitialize();
}

EBit EBit::operator +(const EBit &e) const
{
    EBit    resultEncBit ( m_pk );

    mpz_add(resultEncBit.m_encryption, this->m_encryption, e.m_encryption);
    mpz_mod(resultEncBit.m_encryption, resultEncBit.m_encryption, m_pk->p);


    //resultEncBit.m_dirtiness = this->m_dirtiness +  e.m_dirtiness;
    resultEncBit.m_dirtiness = std::max( this->m_dirtiness, e.m_dirtiness );
    resultEncBit._handleDirtiness( cXORDirtinessComponent ); //waga 'zabrudzenia' ustawiona arbitralnie

    return resultEncBit;
}

EBit EBit::operator *(const EBit &e) const
{
    EBit    resultEncBit ( m_pk );

//    if( (this->m_dirtiness + e.m_dirtiness) > cMaxDirtiness )
//    {
//        if( this->m_dirtiness > e.m_dirtiness )
//        {
//            this->recrypt();

//            if( e.m_dirtiness > (cMaxDirtiness/2) )
//            {
//                e.recrypt();
//            }
//        }
//        else
//        {
//            e.recrypt();

//            if( this->m_dirtiness > (cMaxDirtiness/2) )
//            {
//                this->recrypt();
//            }

//        }
//    }

    mpz_mul(resultEncBit.m_encryption, this->m_encryption, e.m_encryption);
    mpz_mod(resultEncBit.m_encryption, resultEncBit.m_encryption, m_pk->p);

    //resultEncBit.m_dirtiness = this->m_dirtiness *  e.m_dirtiness;
    resultEncBit.m_dirtiness = std::max( this->m_dirtiness, e.m_dirtiness );
    resultEncBit._handleDirtiness( cANDDirtinessComponent ); //waga 'zabrudzenia' ustawiona arbitralnie

//    resultEncBit.m_dirtiness = this->m_dirtiness + e.m_dirtiness;
//    resultEncBit._handleDirtiness( 0 );

    return resultEncBit;
}

EBit EBit::operator ~() const
{
    EBit    resultEncBit ( m_pk );
    const EBit    & oneBias = ProcessorHE::getOneBias();

    mpz_add(resultEncBit.m_encryption, this->m_encryption, oneBias.m_encryption);
    mpz_mod(resultEncBit.m_encryption, resultEncBit.m_encryption, m_pk->p);

    //resultEncBit.m_dirtiness = this->m_dirtiness +  oneBias.m_dirtiness;
    resultEncBit.m_dirtiness = std::max( this->m_dirtiness, oneBias.m_dirtiness );
    resultEncBit._handleDirtiness( cXORDirtinessComponent ); //waga 'zabrudzenia' ustawiona arbitralnie

    return resultEncBit;
}

void    EBit::_initialize()
{
    m_dirtiness = cInitialDiritiness;
    mpz_init( m_encryption );
}

void    EBit::_deinitialize()
{
    mpz_clear( m_encryption );
}

void EBit::_handleDirtiness(int additionalDirtiness)
{
    //powyzej pewnego 'zabrudzenia' trzeba wykonac reszyfrowanie.
    m_dirtiness += additionalDirtiness;
    if( m_dirtiness > cMaxDirtiness )
    {
        recrypt();
    }
}


EBit &EBit::operator =(const EBit &b)
{
    m_pk[0]         = b.m_pk[0];
    m_dirtiness     = b.m_dirtiness;

    mpz_set( m_encryption, b.m_encryption );
    return *this;
}

void EBit::recrypt()
{
    fhe_recrypt( m_encryption, m_pk );
    m_dirtiness = cInitialDiritiness;    //usuniecie 'zabrudzenia'
}

int EBit::decrypt(fhe_sk_t sk)
{
    return fhe_decrypt( m_encryption, sk );
}

void EBit::encrypt(int bitVal)
{
    bitVal = ( bitVal != 0 ? 1 : 0 );
    return fhe_encrypt( m_encryption, m_pk, bitVal );
}

void EBit::print(fhe_sk_t sk)
{
    printf( "Bit: %d\n", decrypt(sk) );
    fflush( stdout );
}

void EBit::fulladd( EBit & sum, EBit & cOut, EBit & a, EBit & b, EBit & cIn )
{
	fhe_fulladd( sum.m_encryption, cOut.m_encryption, a.m_encryption, b.m_encryption, cIn.m_encryption, sum.m_pk );
}

void EBit::toFile (FILE * file) 
{
    int x = mpz_out_raw (file, this->m_encryption);

    if (x == 0)
    {
        printf("Error: mpz_out_raw\n");
    }
}

void EBit::fromFile (FILE *file) 
{
    int x = mpz_inp_raw (this->m_encryption, file);

    if (x == 0)
    {
        printf("Error: mpz_inp_raw\n");
    }
}
