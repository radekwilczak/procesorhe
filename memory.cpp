#include "memory.h"


void    memRead( EWord  & out, const std::vector< EWord > & memory, EWord & select, const EBit & chipSelect )
{
    assert( memory.size() > 0 );
    int memorySize  = memory.size();
    int wordSize    = memory[0].size();
    int selectSize  = select.size();

    assert( wordSize == out.size() );
    assert( selectSize >= memorySize );

    EWord   temp( out.pk(), wordSize, 0 );


    out.elementwiseXOR( out );  //zeruje out

    for( int i = 0 ; i < memorySize; ++i )
    {
        const EWord & word = memory[ i ];
        word.bitAND( temp, select[ i ] );
        out.elementwiseXOR( temp );
    }

    out.bitAND( chipSelect );   //zeruje jesli uklad jest nieaktywny
}

void    memWrite( std::vector< EWord > & memory, EWord & in, EWord & select, const EBit & chipSelect )
{
    assert( memory.size() > 0 );
    int memorySize  = memory.size();
    int wordSize    = memory[0].size();
    int selectSize  = select.size();

    assert( wordSize == in.size() );
    assert( selectSize >= memorySize );

    EWord   temp( in.pk(), wordSize, 0 );

    for( int i = 0 ; i < memorySize; ++i )
    {
        // word = word + ( word + in ) * (chipSelect * select)
        // jesli (chipSelect * select) == 1 to
        // word = word + ( word + in ) * 1 => word = word + word + in  => word = in
        // jesli (chipSelect * select) == 0 to
        // word = word + ( word + in ) * 0 => word = word

        EWord & word = memory[ i ];
        in.elementwiseXOR( temp, word );
        temp.bitAND( ( select[ i ] * chipSelect ) );
        temp.elementwiseXOR( word, word );
    }
}

void    memReadSimple( EWord  & out, const std::vector< EWord > & memory, int select, const EBit & chipSelect )
{
    assert( memory.size() > 0 );
    int memorySize  = memory.size();
    int wordSize    = memory[0].size();


    assert( wordSize == out.size() );

}

void    memWriteSimple( std::vector< EWord > & memory, EWord & in, int select, const EBit & chipSelect )
{
    assert( memory.size() > 0 );
    int memorySize  = memory.size();
    int wordSize    = memory[0].size();

    assert( wordSize == in.size() );

    EWord & memWord = memory[ select ];


}
