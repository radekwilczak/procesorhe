#ifndef ALUFUNCTIONS_H
#define ALUFUNCTIONS_H

#include "eword.h"
#include "ebit.h"

void    aluShiftLeft( EWord & resultA, EWord & inR,  EBit & chipSelect );
void    aluShiftRight( EWord & resultA, EWord & inR,  EBit & chipSelect  );

void    aluXOR( EWord & resultA, EWord & inA, EWord & inR, EBit & chipSelect );
void    aluAND( EWord & resultA, EWord & inA, EWord & inR, EBit & chipSelect );
void    aluNOT( EWord & result, EWord & in, EBit & chipSelect );


void	aluAdd( EWord & resultA, EWord & inA, EWord & inR, EBit & chipSelect  );
void	aluMul( EWord & resultA, EWord & inA, EWord & inR, EBit & chipSelect  );



#endif // ALUFUNCTIONS_H
