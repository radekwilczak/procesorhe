#include "miscfunctions.h"
#include "processor.h"

#include <math.h>

void    miscINC( EWord & out, EWord & in, EBit & chipSelect )
{
    //TODO moze warto uzyc tutaj addera?
    assert( out.size() == in.size() );
    assert( out.size() > 1 );

    int len = out.size();

    EWord   copy( out );

    copy[0] = ~(in[0]);
    for( int i = 1 ; i < len ; ++i )
    {
        EBit    ANDnorm = in[0];
        for( int j = 1 ; j  < i ; ++j )
        {
            ANDnorm = ANDnorm * in[j];
        }

        copy[ i ] = in[ i ] + ANDnorm;
    }

    out = copy;
    out.bitAND( chipSelect );
}


void    miscMUX( EWord & out, EWord & in, int addressBits )
{
    if( addressBits < 0 )
        addressBits = in.size();

    int totalValues = pow( 2, addressBits );
    int totalOuts = ( out.size() < totalValues ? out.size() : totalValues );

    //poroniona wersja z przesunieciami:
    /*EWord   w1( in.pk(), out.size(), 0 );
    EWord   result( in.pk(), out.size(), 0 );
    EWord   shifted( in.pk(), out.size(), 0 );

    w1[0]       = ProcessorHE::getOneBias();
    result[0]   = ProcessorHE::getOneBias();

    for( int i = 0 ; i < addressBits ; ++i )
    {
        int pow2 = pow( 2, i );
        miscShiftN( shifted, result, pow2  );
        shifted.bitAND( in[i] );
        result.bitAND( ~in[i] );

        result.elementwiseXOR( shifted );
    }

    EWord   negIn( in.pk(), in.size(), 0 );
    in.negated( negIn );
    EBit zeroAddr =  negIn.ANDnorm( 0, addressBits );
    result[0] = result[0] * zeroAddr;

    result.copyTo( out );*/

    //wersja z mnozeniem, prosta lekka i przyjemna
    for( int i = 0 ; i < totalOuts ; ++i )
    {
        EBit    ANDnorm( in.pk(), 1 );
        int     value = i;
        for( int j = 0 ; j < addressBits ; ++ j)
        {
            int bit = value & 0x01;
            value = value >> 1;

            if( bit )
            {
                ANDnorm = ANDnorm*in[j];
            }
            else
            {
                ANDnorm = ANDnorm * (~in[j]);
            }
        }
        out[ i ] = ANDnorm;
    }
}


void    miscShiftN( EWord & out, EWord & in, int nPos)
{
    assert( out.size() == in.size() );

    int len = in.size();
    for( int i = 0 ; i < len ; ++i )
    {
        int fromIndex = i - nPos;
        if( fromIndex >= 0 && fromIndex < len )
        {
            out[ i ] = in[ fromIndex ];
        }
        else
        {
            out[ i ] = ProcessorHE::getZeroBias();
        }
    }
}


EBit    miscAnyBitOne( EWord & in, int indexes[], unsigned int indexesCount )
{
    EWord   inNegated( in.pk(), in.size() );
    EBit    result( in.pk(), 1 );

    in.negated( inNegated );

    for( int i = 0 ; i < indexesCount ; ++i )
    {
        int index = indexes[ i ];
        result = result * inNegated[ index ];
    }

    return (~result);
}
