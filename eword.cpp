#include "eword.h"
#include <algorithm>
#include <stdio.h>

EWord::EWord(fhe_pk_t pk, int len, const EBit &initialBit)
{
    m_N = len;
    m_pk[0] = pk[0];
    for( int i = 0 ; i < m_N ; ++i )
    {
        m_bits.push_back( initialBit );
    }
}

EWord::EWord( fhe_pk_t pk, int len, int initialValue   )
{
    m_N = len;
    m_pk[0] = pk[0];
    for( int i = 0 ; i < m_N ; ++i )
    {
        m_bits.push_back( EBit(pk, initialValue ));
    }
}

EWord::EWord(fhe_pk_t pk, int len)
{
    m_N = len;
    m_pk[0] = pk[0];
    for( int i = 0 ; i < m_N ; ++i )
    {
        m_bits.push_back( EBit(pk ));
    }
}

EWord::EWord(const EWord &word)
{
    *this = word;
}

EWord &EWord::operator =(const EWord &word)
{
    m_N = word.m_N;
    m_pk[0] = word.m_pk[0];
    m_bits = word.m_bits;

    return *this;
}

unsigned int EWord::size() const
{
    return m_bits.size();
}

EBit &EWord::operator [](int i)
{
    return m_bits[ i ];
}

const EBit &EWord::operator [](int i) const
{
    return m_bits[ i ];
}

void EWord::copyTo(EWord &dest, int srcOffset, int dstOffset, int length)
{
    int toCopy = 0;

    toCopy = length;
    if( toCopy < 0 )
        toCopy = std::min( this->size() - srcOffset, dest.size() - dstOffset );

    //TODO: assert

    for( int i = 0 ; i < toCopy; ++i )
    {
        dest[i + dstOffset] = (*this)[i + srcOffset];
    }
}

void EWord::encodeUInt(unsigned int value)
{
    for( int i = 0 ; i < m_N ; ++i )
    {
        int bit = value & 0x01;
        value = value >> 1;
        m_bits[i].encrypt( bit );
    }

}

unsigned int EWord::decodeUInt(fhe_sk_t sk )
{
    unsigned int resultValue = 0;
    for( int i = m_N - 1 ; i >= 0 ; --i )
    {
        resultValue <<= 1;
        int bit = m_bits[i].decrypt( sk );
        resultValue |= bit;
    }
    return resultValue;
}

void EWord::encodeBits(const int bits[], int offset, int size)
{
    //TODO
}

void EWord::decodeBits(int bits[], fhe_sk_t s, int offset, int size)
{
    //TODO
}

void EWord::bitAND(const EBit &bit)
{
    for( int i = 0 ; i < m_N; ++i )
    {
        m_bits[i] = m_bits[i] * bit;
    }
}

void EWord::bitAND(EWord &result, const EBit &bit) const
{
    assert( result.size() == this->size() );

    for( int i = 0 ; i < m_N; ++i )
    {
        result[i] = m_bits[i] * bit;
    }
}

void EWord::elementwiseXOR(EWord &result, const EWord &in) const
{
    assert( result.size() == in.size() );
    assert( this->size() == in.size() );
    for( int i = 0 ; i < m_N ; ++i )
    {
        result[ i ] = (*this)[i] + in[i];
    }
}

void EWord::elementwiseXOR(const EWord &in)
{
    assert( this->size() == in.size() );

    for( int i = 0 ; i < m_N ; ++i )
    {
        (*this)[i] = (*this)[i] + in[i];
    }
}

void EWord::negate()
{
    for( int i = 0 ; i < m_N ; ++i )
    {
        (*this)[i] = ~ ( (*this)[i] );
    }
}

void EWord::negated(EWord &negated) const
{
    assert( negated.size() == this->size() );

    for( int i = 0 ; i < m_N ; ++i )
    {
        negated[i] = ~ ( (*this)[i] );
    }
}

EBit EWord::ANDnorm() const
{
    EBit resultBit( m_pk, 1 );

    for( int i = 0 ; i < m_N ; ++i )
    {
        resultBit = resultBit * (*this)[i];
    }

    return resultBit;
}

EBit EWord::ANDnorm(unsigned int offset, unsigned int size) const
{
    EBit resultBit( m_pk, 1 );

    for( int i = 0 ; i < size ; ++i )
    {
        resultBit = resultBit * (*this)[ i + offset ];
    }

    return resultBit;
}

EBit EWord::XORnorm()
{
    EBit resultBit( m_pk, 1 );

    for( int i = 0 ; i < m_N ; ++i )
    {
        resultBit = resultBit + (*this)[i];
    }

    return resultBit;
}

void EWord::print(fhe_sk_t sk)
{
    printf( "bits:" );
    for( int i = 0 ; i < m_N; ++i )
    {
        printf( " %d", m_bits[ i ].decrypt( sk ) );
    }
    printf( "\n" );
    fflush( stdout );
}

void EWord::toFile(FILE *file)
{
    int wordSize = this->size();

    fwrite ( &wordSize, sizeof(int), 1, file );

    for ( int i = 0 ; i < wordSize ; ++i )
    {
        EBit bit = (*this)[i];
        bit.toFile(file);
    }
}

void EWord::fromFile(FILE *file)
{
    int wordSize;

    // wczytanie długości słowa
    fread ( &wordSize, sizeof(int), 1, file );

    // usuwanie wszystkich m_bits
    for( int i = 0 ; i < m_N ; ++i )
    {
        this->m_bits.pop_back();
    }

    m_N = wordSize;

    // wczytywanie wszystkich bitów
    for( int i = 0 ; i < m_N ; ++i )
    {
        EBit bit(m_pk);
        bit.fromFile(file);
        this->m_bits.push_back(bit);
    }
}
