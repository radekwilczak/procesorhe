#ifndef TOFILE_H
#define TOFILE_H

#include "eword.h"

class ToFile
{
public:

	static void wordToFile (EWord & word, char *filename);
	static void wordFromFile (EWord & word, char *filename);

    static bool readMemory( std::vector< EWord > & out, const char * filename, fhe_pk_t & pk );
    static bool writeMemory( const char * filename, const std::vector< EWord > & in );

    static bool readPK( const char * filename, fhe_pk_t & outPK );
    static bool readSK( const char * filename, fhe_sk_t & outSK );

    static bool writePK( const char * filename, const fhe_pk_t & inPK );
    static bool writeSK( const char * filename, const fhe_sk_t & inSK );
};

#endif // TOFILE_H
