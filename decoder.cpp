
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>

#include "eword.h"
#include "tofile.h"

using namespace std;


int main( int argc, char * args[] )
{
    fhe_pk_t pk;
    fhe_sk_t sk;

    string inMemoryFileName( "resultData.mem" );
    string outMemoryFileName( "resultData.txt" );
    fstream memFile;
    vector< EWord > dataMemory;

    memFile.open( outMemoryFileName.c_str(), ios_base::out | ios_base::trunc );

    if( !memFile.is_open() )
    {
        printf( "fail to open input code file %s\n", outMemoryFileName.c_str() );
        return 1;
    }


    ToFile::readPK( "pk.pk", pk );
    ToFile::readSK( "sk.sk", sk );

    ToFile::readMemory( dataMemory, inMemoryFileName.c_str() , pk );

    int dataWordsCount = dataMemory.size();
    int dataWordSize = dataMemory[0].size();

    memFile << dataWordsCount << " " << dataWordSize << endl;
    for( int i = 0 ; i < dataMemory.size() ; ++i )
    {
        unsigned int decoded = dataMemory[i].decodeUInt( sk );
        printf( "data word = %d\n", decoded );
        memFile << decoded << endl;
    }

    fhe_pk_clear( pk );
    fhe_sk_clear( sk );

    memFile.close();

    return 0;
}
