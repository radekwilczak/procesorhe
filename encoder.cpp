#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>

#include "eword.h"
#include "tofile.h"

using namespace std;


int main( int argc, char * args[] )
{
//    if( argc < 2 )
//    {
//        printf( "Brak pliku z kodem programu\n" );
//        return 1;
//    }

    fhe_pk_t pk;
    fhe_sk_t sk;

    fhe_pk_init( pk );
    fhe_sk_init( sk );

    fhe_keygen( pk, sk );
    printf( "generating keys\n" );

    string codeFileName( "code.txt" );
    string memoryFileName( "data.txt" );
    fstream codeFile;
    fstream memFile;

    codeFile.open( codeFileName.c_str() , ios_base::in );
    memFile.open( memoryFileName.c_str(), ios_base::in );

    if( !memFile.is_open() )
    {
        printf( "fail to open input code file %s\n", codeFileName.c_str() );
        return 1;
    }

    if( !memFile.is_open() )
    {
        printf( "fail to open input code file %s\n", memoryFileName.c_str() );
        return 1;
    }

    int codeWordsCount;
    int codeWordSize;
    int dataWordsCount;
    int dataWordSize;
    vector< EWord > codeMemory;
    vector< EWord > dataMemory;

    codeFile >> codeWordsCount;
    codeFile >> codeWordSize;

    for( int i = 0 ; i < codeWordsCount ; ++i )
    {
        if( !codeFile )
            break;

        unsigned int inCodeWord;
        codeFile >> inCodeWord;
        EWord codeWord( pk, codeWordSize  );
        codeWord.encodeUInt( inCodeWord );
        codeMemory.push_back( codeWord );
    }
    codeFile.close();

    memFile >> dataWordsCount;
    memFile >> dataWordSize;
    for( int i = 0 ; i < dataWordsCount ; ++i )
    {
        if( !codeFile )
            break;

        unsigned int inDataWord;
        memFile >> inDataWord;
        EWord dataWord( pk, dataWordSize  );
        dataWord.encodeUInt( inDataWord );
        dataMemory.push_back( dataWord );
    }
    memFile.close();

    printf( "saving  pk\n" );
    ToFile::writePK( "pk.pk", pk );
    printf( "saving  sk\n" );
    ToFile::writeSK( "sk.sk", sk );

    printf( "saving  code memory\n" );
    ToFile::writeMemory( "code.mem", codeMemory );
    printf( "saving  data memory\n" );
    ToFile::writeMemory( "data.mem", dataMemory );

    fhe_pk_clear( pk );
    fhe_sk_clear( sk );
    printf( "written\n" );

    {
        fhe_pk_t pk2;
        fhe_sk_t sk2;

        vector< EWord > rCodeMemory;
        vector< EWord > rDataMemory;

        ToFile::readPK( "pk.pk", pk2 );
        ToFile::readSK( "sk.sk", sk2 );

        ToFile::readMemory( rCodeMemory, "code.mem", pk2 );
        ToFile::readMemory( rDataMemory, "data.mem", pk2 );

        printf( "code memory check\n" );
        for( int i = 0 ; i < rCodeMemory.size() ; ++i )
        {
            unsigned int decoded = rCodeMemory[i].decodeUInt( sk2 );
            printf( "code word = %d\n", decoded );
        }

        printf( "data memory check\n" );
        for( int i = 0 ; i < rDataMemory.size() ; ++i )
        {
            unsigned int decoded = rDataMemory[i].decodeUInt( sk2 );
            printf( "data word = %d\n", decoded );
        }

        fhe_pk_clear( pk2 );
        fhe_sk_clear( sk2 );
    }

    return 0;
}
